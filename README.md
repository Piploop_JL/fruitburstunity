# Fruit Burst

Fruit Burst was originally an old programming assignment that I remade using Unity as a Game Engine.

## Setting Up

Download the zip file, extract it, and run the FruitBurst.exe file.
If everything is correct, it should run the game just fine~

## How to Play

Click on the fruits to gain or lose points.
You have 120 seconds to earn as many points as you can.

Different fruits have different values:

Blue Round Berries: 1 points

Blue Raspberries: -5 points

Yellow Berries: 3 points (can be clicked up to 5 times)

Peach Berries: -10 points (can be clicked up to 5 times)





You will instantly lose if you have less than 0 points!
Have Fun!

